export class User {
    id: number;
    username:string;
    password:string;
    emailid:string;
    phoneno:number;
    bankaccountnumber:string;
    usertype:string;
    balance:number;
    isactive: boolean;
    address:string;
    
}
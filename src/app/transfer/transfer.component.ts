import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { CurrentuserService } from '../currentuser.service';
import { Transaction } from '../transaction';
import { TransactionService } from '../transaction.service';
import { User } from '../user';
import { UserService } from '../user.service';
import {Location} from '@angular/common';
@Component({
  selector: 'app-transfer',
  templateUrl: './transfer.component.html',
  styleUrls: ['./transfer.component.css']
})
export class TransferComponent implements OnInit {

  users:User[]=[];
  users1:User[]=[];
  user:User = new User();
  user1:User = new User();
  submitted = false;
 fromaccountnumber;
  sender;
  toaccountnumber;
  receiver;
  amount;
  date=new Date();
  transaction:Transaction=new Transaction();
    constructor(private formBuilder: FormBuilder,public us:UserService,public ts:TransactionService,currentUser:CurrentuserService
      ,private router:Location) { 
      
      this.user=currentUser.getUserData();
    currentUser.setUserData(this.user);
    }

  ngOnInit(): void {
  }
  transfer= new FormGroup({
    toaccountnumber: new FormControl('', Validators.required),
    transactionamount: new FormControl('', Validators.required),
    transactionControl:new FormControl('',Validators.required)
  });
  get f() { return this.transfer.controls; }
  isDebitSelected: boolean;
  selectInput(event) {
    let selected = event.target.value;
    if (selected == "debit") {
      this.isDebitSelected = true;
    } else {
      this.isDebitSelected = false;
    }
  }
  onSubmit(){
    if(this.isDebitSelected==true){
      
    this.us.getUsers().subscribe(res=>{
      res.forEach(res=>{
        this.users.push(res);
        console.log(this.users);
      })

      let user2=this.users.filter(res=>{
      return res.bankaccountnumber==this.transfer.get('toaccountnumber').value
    });
    console.log(user2);
    if(user2[0].bankaccountnumber!=this.user.bankaccountnumber){
    if(this.user.balance>=this.transfer.get('transactionamount').value){
      user2[0].balance=user2[0].balance+this.transfer.get('transactionamount').value;
      this.user.balance=this.user.balance-this.transfer.get('transactionamount').value;
      this.us.update(this.user.id,this.user).subscribe();
      this.us.update(user2[0].id,user2[0]).subscribe();
      this.transaction.fromaccountnumber=this.user.bankaccountnumber;
      this.transaction.toaccountnumber=user2[0].bankaccountnumber;
      this.transaction.transactionamount=this.transfer.get('transactionamount').value;
      this.transaction.transactiondate=this.date;
      this.transaction.balanceondate=this.user.balance-this.transfer.get('transactionamount').value;
      this.transaction.remarks="Transaction Successfull";
      this.transaction.transactiontype="debit";
      this.ts.createTransaction(this.transaction).subscribe();
      alert('Transaction Successfull..\n Amount credited  to '+user2[0].username+' with account number '+this.transaction.toaccountnumber);
    }}
    else{
      alert('self transaction can not be done through debit');
    }
    
    }); }
    else{
      this.us.getUsers().subscribe(res=>{
        res.forEach(res=>{
          this.users.push(res);
          
        })
      if(this.user.balance>=this.transfer.get('transactionamount').value){
        this.user.balance=this.user.balance+this.transfer.get('transactionamount').value;
        this.us.update(this.user.id,this.user).subscribe();
        this.transaction.fromaccountnumber=this.user.bankaccountnumber;
        this.transaction.toaccountnumber=this.user.bankaccountnumber;
        this.transaction.transactionamount=this.transfer.get('transactionamount').value;
        this.transaction.transactiondate=this.date;
        this.transaction.balanceondate=this.user.balance+this.transfer.get('transactionamount').value;
      this.transaction.remarks="Transaction Successfull";
      this.transaction.transactiontype="credit";
        this.ts.createTransaction(this.transaction).subscribe();
        alert('Credit Successfull');
      }else{
        alert('You cannot credit more than available amount');
      }
      
      });
    }
    
  }
  navBack(){
    this.router.back();
  }
  

}
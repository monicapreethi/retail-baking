export class Transaction {
    id:number;
    toaccountnumber:string;
    fromaccountnumber:string;
    transactiondate:Date;
    remarks:string;
    transactiontype:string;
    balanceondate:number;
    transactionamount:number;
    status:string;
}
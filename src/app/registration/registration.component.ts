import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { User } from '../user';
import { UserService } from '../user.service';


@Component({
  selector: 'app-registration',
  templateUrl: './registration.component.html',
  styleUrls: ['./registration.component.css']
})
export class RegistrationComponent implements OnInit {

  submitted = false;
  users:User[]=[];
  user:User=new User();
  constructor(private formBuilder: FormBuilder,public us:UserService,public router:Router) { }

  ngOnInit() { }
  registerForm= new FormGroup({
    username: new FormControl('', Validators.required),
    phoneno: new FormControl('', [Validators.required,Validators.minLength(10)]),
    emailid: new FormControl('', [Validators.required, Validators.email]),
    password: new FormControl('', [Validators.required, Validators.minLength(6)]),
    confirmPassword:new FormControl('',[Validators.required,Validators.minLength(6)]),
    address :new FormControl('',[Validators.required])
  });
 
  // convenience getter for easy access to form fields
  get f() { return this.registerForm.controls; }

  onSubmit() {
      this.submitted = true;

      // stop here if form is invalid
      if (this.registerForm.invalid) {
          return;
      }
      else{
        
    let pass = this.registerForm.get('password').value
    let confirmPass = this.registerForm.get('confirmPassword').value;
    console.log(pass);
    console.log(confirmPass);
    if(pass == confirmPass){
        this.user.emailid=this.registerForm.get('emailid').value;
        this.user.username=this.registerForm.get('username').value;
        this.user.phoneno=this.registerForm.get('phoneno').value;
        this.user.password=this.registerForm.get('password').value;
        this.user.isactive=false;
        this.user.usertype='';
        this.user.bankaccountnumber='';
        this.user.balance=0;
        console.log(this.user);
        this.us.createUsers(this.user).subscribe();
        this.registerForm.reset();
       alert("Registration successfull  now login");
        this.router.navigate(['login']);
    }
    else{
          alert("Password and confirm password not match.");
        }
      }
  }

}
import { Component, OnInit } from '@angular/core';

import{Location} from '@angular/common';
import * as XLSX from 'xlsx';
import { TransactionService } from '../transaction.service';

@Component({
  selector: 'app-transactionlist',
  templateUrl: './transactionlist.component.html',
  styleUrls: ['./transactionlist.component.css']
})
export class TransactionlistComponent implements OnInit {
   transactions;
   fromaccountnumber;
   toaccountnumber;
   sender;
   fileName="all_transactions.xlsx";
  constructor(public ts:TransactionService,private router:Location) { }

  ngOnInit() {
    this.ts.getTransactions().subscribe(res=>{
      this.transactions=res;
    })
  }
 
    
  
  
  navBack(){
    this.router.back();
  }
    
exportexcel(): void 
 {​​​​​​​​
 /* table id is passed over here */ 
 let element = document.getElementById('fileName'); 
 const ws: XLSX.WorkSheet =XLSX.utils.table_to_sheet(element);

 /* generate workbook and add the worksheet */
 const wb: XLSX.WorkBook = XLSX.utils.book_new();
 XLSX.utils.book_append_sheet(wb, ws, 'Sheet1');

 /* save to file */
 XLSX.writeFile(wb, this.fileName);

 }​​​​​​​​


}
